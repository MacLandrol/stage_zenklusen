#!/usr/bin/perl
use List::Util qw( min max );

#Check if the fasta file is an arguments
if(scalar @ARGV<1){
	print "Sequence not found\n";
	exit();
}

#Call to clustalo from system
$inputSeq =$ARGV[0];
system('clustalo -i '.$inputSeq.' --infmt=fasta --out res.fa --outfmt=fa --force');
open FILE, '<', "res.fa" or die $!;

my %sequence; #contains all aligned sequence name and the corresponding sequence
my @name; #list of all the name (>name)
my $count=-1;
my %id; #identity score hash
my $seqlen=0;

#Read file and extract data
while ($line=<FILE>) {
	chomp $line;
	if(substr($line,0,1) eq ">"){
		$count+=1;
		$name[$count]=substr($line,1);
		$sequence{$name[$count]}='';
	}
	else{
		if($count>=0){
			$sequence{$name[$count]}.=$line;
		}

	}

}
if(scalar (@name)>0){
	$seqlen=length($sequence{$name[0]});
}
my $cons="";

#loop over each charactere of the one of the sequence
# assuming same length after alignment
for (my $i=0; $i<$seqlen; $i++){
	my %freq ={};
	$max=0;
	foreach(map{substr($sequence{$_},$i,1)} @name){
		# Count the occurence of the charactere in the list of sequence
		if(exists $freq{$_}){
			$freq{$_}++;
		}
		else{
			$freq{$_}=1;
		}
	}
	$max= max (values %freq);
	my @res= grep {$freq{$_}== $max} keys (%freq);
	
	#Debuging
	#print "val\t", values(%freq), "\tmax:", $max, "\tlen(@res)\t", scalar(@res),"\n";
	
	#Find consensus charactere.
	if(scalar (@res)>1){
		$char="?";
	}
	else{
		$char=shift(@res);
	}
	$cons.=$char;
	
	#Computing identity score
	foreach (@name){
		$seqCh=substr($sequence{$_},$i,1);
		if($seqCh eq $char){
			if(exists $id{$_}){
				$id{$_}++;
			}
			else{
				$id{$_}=1;
			}	
		}
	}
	
}

#Output result
$sequence{"consensus"}=$cons;
print "**Séquence consensus:\n".$cons, "\n\n**Pourcentage d'identité: \n";
print $_.": ".sprintf("%.2f%%", 100*$id{$_}/$seqlen)."\n" for keys(%id);

#Finding foreign sequence 
$seuil =0.5;
my @retenus = grep {($id{$_}/$seqlen)>=$seuil} keys %id;
print "\n**Séquences retenues :\n", join(", ", @retenus), "\n";